import React from 'react';
import cx from 'classnames';
import { CircularProgress } from '@material-ui/core';
import './Page.scss';

export const Page = ({ heading, buttons, children, loading, error }) => (
  <div className={cx('Page', { 'Page--loading': loading, 'Page--error': error })}>
    <header>
      <h1 className="Page-heading">
        {heading}
      </h1>
      <div className="Page-buttons">
        {buttons}
      </div>
    </header>
    <div className="Page-content">
      {
        loading ? <CircularProgress /> : error ? <div>There was error loading your page.</div> : children
      }
    </div>
  </div>
);