import React, { useState, useEffect } from 'react';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { toCapitalCase } from '@Utils';
import { Button } from '@material-ui/core';
import { Page } from '@Components/Page/Page';
import './UserEdit.scss';

const ROLES = [
  'ADMIN',
  'DEVELOPER',
  'APP_MANAGER',
  'MARKETING',
  'SALES'
];

const FETCH_USER_QUERY = gql`
  query user($email: ID!) {
    user(email: $email) {
      email
      name
      role
    }
  }
`;

const UPDATE_USER_MUTATION = gql`
  mutation updateUser($email: ID!, $newAttributes: UserAttributesInput!) {
    updateUser(email: $email, newAttributes: $newAttributes) {
      email
      name
      role
    }
  }
`;

export const UserEdit = ({ match, history }) => {
  const [fetchUser, { loading, error, data={} }] = useLazyQuery(FETCH_USER_QUERY, { fetchPolicy: 'no-cache' });
  const [updateUser] = useMutation(UPDATE_USER_MUTATION);
  const { user={ name: '', role: null } } = data;
  const email = decodeURIComponent(match.params.id);
  const [role, setRole] = useState(user.role);
  const [name, setName] = useState(user.name);
  const userChanged = name !== user.name || role !== user.role;

  useEffect(() => {
    fetchUser({ variables: { email } });
  }, [fetchUser, email]);

  useEffect(() => {
    setRole(user.role);
    setName(user.name);
  }, [user]);

  return (
    <Page
      heading={email}
      buttons={
        <Button
          variant="contained"
          color="primary"
          size="small"
          disabled={!userChanged}
          onClick={() => {
            updateUser({ variables: { email: email, newAttributes: { role, name } } })
              .then(() => history.push('/'));
          }}
        >Save</Button>
      }
      loading={loading}
      error={error}
    >
      <div className="UserEdit">
        <div className="UserEdit-name">
          <label>
            Name
            <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
          </label>
        </div>
        <div className="UserEdit-role">
          <label>Role</label>
          {
            ROLES.map((r) => (
              <label key={r}>
                <input
                  type="radio"
                  name="role"
                  value={r}
                  checked={r === role}
                  onClick={(e) => setRole(e.target.value)}
                  readOnly
                /> {toCapitalCase(r.toLocaleLowerCase())}
              </label>
            ))
          }
        </div>
      </div>
    </Page>
  );
};