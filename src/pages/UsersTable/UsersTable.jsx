import React, { useState, useEffect } from 'react';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Button } from '@material-ui/core';
import { DataGrid } from '@material-ui/data-grid';
import { toCapitalCase } from '@Utils';
import { get } from 'lodash';
import { Page } from '@Components/Page/Page';
import './UsersTable.scss';

const columns = [
  {
    field: 'selected',
    width: 35,
    renderCell: ({ value }) => (
      <input type="checkbox" checked={Boolean(value)} readOnly/>
    )
  },
  {
    field: 'email',
    headerName: 'Email',
    width: 350,
    renderCell: ({ value, data }) => (
      <span style={{ color: data.selected ? '#0070c9' : undefined }}>{ value }</span>
    )
  },
  {
    field: 'name',
    headerName: 'Name',
    width: 320
  },
  {
    field: 'role',
    headerName: 'Role',
    width: 140,
    valueGetter: (params) => toCapitalCase(params.getValue('role').toLowerCase())
  }
];

function normalizeRowData(rowData=[], selectedRowKeys, rowKey='id') {
  return rowData.map((row) => ({ id: row[rowKey], selected: selectedRowKeys.includes(row[rowKey]), ...row }));
}

const ALL_USERS_QUERY = gql`
  query {
    allUsers {
      email
      name
      role
    }
  }
`;

const DELETE_USERS_MUTATION = gql`
  mutation deleteUsers($emails: [ID]!) {
    deleteUsers(emails: $emails)
  }
`;

export const UsersTable = ({ history }) => {
  const [fetchUsers, allUsersQuery] = useLazyQuery(ALL_USERS_QUERY, { fetchPolicy: 'no-cache' });
  const [deleteUsers, deleteUsersMutation] = useMutation(DELETE_USERS_MUTATION);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  function toggleSelected(id) {
    const newSelected = selected.includes(id) ? selected.filter((_) => _ !== id) : [ ...selected, id ];

    setSelected(newSelected);
  }

  function onCellClick({ field, data }) {
    switch (field) {
    case 'selected': return toggleSelected(data.id);
    default: return history.push(`/user/${encodeURIComponent(data.id)}`);
    }
  }

  return (
    <Page
      heading="Users"
      buttons={
        <Button
          variant="outlined"
          color="secondary"
          disabled={selected.length === 0}
          onClick={() => {
            deleteUsers({ variables: { emails: selected } })
              .then(() => fetchUsers());
          }}
          size="small"
        >Delete</Button>
      }
    >
      <DataGrid
        className="UsersTable"
        columns={columns}
        loading={allUsersQuery.loading || deleteUsersMutation.loading}
        error={allUsersQuery.error || deleteUsersMutation.error}
        rows={normalizeRowData(get(allUsersQuery,'data.allUsers', []), selected, 'email')}
        onCellClick={onCellClick}
        headerHeight={32}
        rowHeight={43}
        disableSelectionOnClick
        hideFooter
        autoHeight
      />
    </Page>
  );
};