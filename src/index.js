import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes';

const client = new ApolloClient({
  uri: process.env.GRAPHQL_ENDPOINT,
  request: operation => {
    operation.setContext({
      headers: {
        'x-api-key': process.env.GRAPHQL_API_KEY,
      }
    });
  }
});

const Root = () => (
  <ApolloProvider client={client}>
    <Routes />
  </ApolloProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));