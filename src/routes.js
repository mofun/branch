import React, { useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { UsersTable, UserEdit } from '@Pages';
import { createBrowserHistory } from 'history';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import './index.scss';

const RESET_USERS_MUTATION = gql`
  mutation resetUsers {
    resetUsers
  }
`;

const history = createBrowserHistory();

const Routes = () => {
  const [resetUsers] = useMutation(RESET_USERS_MUTATION);

  useEffect(() => {
    resetUsers();
  }, [resetUsers]);

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={UsersTable}/>
        <Route exact path="/user/:id" component={UserEdit}/>
        <Route path="*" component={() => <Redirect to="/"/>}/>
      </Switch>
    </Router>
  );
};

export default Routes;