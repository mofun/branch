import { toCapitalCase } from '../toCapitalCase';

describe('toCapitalCase', () => {
  it('should return string in Capital Case', () => {
    expect(toCapitalCase(null)).toEqual('');
    expect(toCapitalCase('hello')).toEqual('Hello');
    expect(toCapitalCase('Hello There')).toEqual('Hello There');
    expect(toCapitalCase('hello there')).toEqual('Hello There');
    expect(toCapitalCase('hello_there  people')).toEqual('Hello There People');
    expect(toCapitalCase(' The.people-areCool ')).toEqual('The People Are Cool');
  });
});