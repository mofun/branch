export function toCapitalCase(str) {
  const formattedStr = str ? str.replace(/^\w|[/._-\s]{1,}\w|[A-Z]/g, (_) => ` ${ _.slice(-1).toUpperCase() }`) : '';

  return formattedStr.trim();
}